function affiche(i) {
    $('#titre').html(exemples[i].nom);
    $.ajax({
        mimeType: 'text/plain;',
        url: 'assets/' + exemples[i].python,
        dataType: "text"
    }).done(function (data) {
        $('#code').html(data);
        document.querySelectorAll('#code').forEach(function (block) {
            hljs.highlightBlock(block);
            hljs.lineNumbersBlock(block);
        });
        let url = "https://pythontutor.com/visualize.html#code=";
        url = url + encodeURIComponent(data);
        url = url + "&cumulative=false&heapPrimitives=nevernest&mode=edit&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false";
        let action = 'window.open("'+url+'")';
        document.getElementById('pythontutorButton').setAttribute('onclick',action);       
    });
    $.ajax({
        mimeType: 'text/html',
        url: 'assets/' + exemples[i].comment,
        dataType: "html"
    }).done(function (data) {
        $('#comment').html(data);
        document.querySelectorAll('#comment pre code').forEach(function (block) {
            hljs.highlightBlock(block);
        });
    });

    if (typeof exemples[i].codepuzzle !=='undefined') {
        let url = "https://www.codepuzzle.io/";
        url = url + exemples[i].codepuzzle;
        let action = 'window.open("'+url+'")';
        document.getElementById('codePuzzleButton').setAttribute('onclick',action);
        $('#codePuzzleButton').removeClass('d-none');
        $('#codePuzzleButton').addClass('d-block');
    } else {
        $('#codePuzzleButton').removeClass('d-block');
        $('#codePuzzleButton').addClass('d-none');
    }
        

}

function initialise() {
    let n = 0;
    exemples.forEach(function (e, i) {
        if (e.tags.includes('title')) {
            lien = "<span class=\"separators\">";
            lien += e.nom;
            lien += "</span>";
        } else {
            n = n + 1;
            lien = "<a class=\"dropdown-item\" href=\"#\" onclick=\"";
            lien += "affiche(" + i + ")";
            lien += "\">" + n + ". " + e.nom + "</a>";
        }
        $('#listeExemples').append(lien);
    });
    affiche(1);
}