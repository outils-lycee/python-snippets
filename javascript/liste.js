exemples=[
    {
        nom:'Base du langage',
        tags:['title']
    },
    {
        nom:'Arithmétique ',
        python:'01_arithmetique.py',
        comment:'01_arithmetique.html',
        tags:[]
    },{
        nom:'Division entière',
        python:'02_euclidienne.py',
        comment:'02_euclidienne.html',
        tags:[]
    },{
        nom:"Chaînes de caractères",
        python:"03_strings.py",
        comment:'03_strings.html',
        tags:[]
    },{
        nom:'Boucle While',
        python:'04_while.py',
        comment:'04_while.html',
        tags:[]
    },{
        nom:'Interaction avec input',
        python:'05_input.py',
        comment:'05_input.html',
        tags:[]
    },{
        nom:'Test d\'age',
        python:'06_age.py',
        comment:'06_age.html',
        tags:[]
    },{
        nom:'Test de parité',
        python:'07_parite.py',
        comment:'07_parite.html',
        tags:[]
    },{
        nom:'Créer et manipuler une liste',
        python:'08_liste.py',
        comment:'08_liste.html',
        tags:[]
    },{
        nom:'Peupler une liste',
        python:'09_liste_while.py',
        comment:'09_liste_while.html',
        tags:[]
    },{
        nom:'Parcours de liste (while)',
        python:'10_parcours_liste_while.py',
        comment:'10_parcours_liste_while.html',
        tags:[]
    },{
        nom:'Parcours de liste (for)',
        python:'11_parcours_liste_for.py',
        comment:'11_parcours_liste_for.html',
        tags:[]
    },{
        nom:'Définir une fonction',
        python:'12_fonction.py',
        comment:'12_fonction.html',
        tags:[]
    },{
        nom:'La fonction range',
        tags:['title']
    },{
        nom:'Range : usage simple',
        python:'13_range.py',
        comment:'13_range.html',
        tags:[]
    },{
        nom:'Range : répéter n fois',
        python:'13.1_range_for.py',
        comment:'13.1_range_for.html',
        tags:[]
    },{
        nom:'Range : parcours de liste',
        python:'17_range_parcours.py',
        comment:'17_range_parcours.html',
        tags:[]
    },{
        nom:'Range : usage avec début et fin',
        python:'14_range_2.py',
        comment:'14_range_2.html',
        tags:[]
    },{
        nom:'Range : usage avec début, fin et pas',
        python:'15_range_3.py',
        comment:'15_range_3.html',
        tags:[]
    },{
        nom:'Range : un compte à rebours',
        python:'16_rebours.py',
        comment:'16_rebours.html',
        tags:[]
    },{
        nom:'Liste par compréhension &#x2B50;',
        tags:['title']
    },{
        nom:'Créer une liste par compréhension',
        python:'18_comprehension.py',
        comment:'18_comprehension.html',
        tags:[]
    },{
        nom:'Filtrer une liste par compréhension',
        python:'19_comprehension_if.py',
        comment:'19_comprehension_if.html',
        tags:[]
    },{
        nom:'Compréhension et range',
        python:'20_comprehension_range.py',
        comment:'20_comprehension_range.html',
        tags:[]
    },{
        nom:'Algorithmes de recherche usuels',
        tags:['title']
    },{
        nom:'Minimum d\'une liste',
        python:'21_minimum.py',
        comment:'21_minimum.html',
        tags:[],
        codepuzzle:'PZUXE'
    },{
        nom:'Position du minimum d\'une liste',
        python:'22_position_minimum.py',
        comment:'22_position_minimum.html',
        tags:[],
        codepuzzle:'PGRT8'
    },{
        nom:'Algorithmes de tri &#x2B50; &#x2B50;',
        tags:['title']
    },{
        nom:'tri par sélection',
        python:'23_tri_selection.py',
        comment:'23_tri_selection.html',
        tags:[],
        codepuzzle:'PLF9V'
    },{
        nom:'tri par insertion',
        python:'24_tri_insertion.py',
        comment:'24_tri_insertion.html',
        tags:[],
        codepuzzle:'PXNCG'
    }
];