#!/usr/bin/python3

n = int(input("Donnez un nombre entier."))

if n % 2 == 0:
    print(str(n) + " est pair ")
else:
    print(str(n) + " est impair")