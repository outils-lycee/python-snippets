#!/usr/bin/python3

def somme_carres(n):
    somme = 0
    i=1
    while i<=n:
        somme = somme + i**2
        i = i + 1
         
    return somme

# Utilisation de la fonction
print("la somme des carrés de 1 à 12 est : ")
print(somme_carres(12))