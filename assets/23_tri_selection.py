# /usr/bin/python3

def tri_selection(tab):
    t = list(tab)
    trie = 0
    while trie < len(t) - 1 :
        pos_min = trie
        for pos in range(trie,len(t)):
            if t[pos] < t[pos_min]:
                pos_min = pos
        t[trie], t[pos_min] = t[pos_min], t[trie]
        trie += 1
    return t
