# /usr/bin/python3

def tri_insertion(tab):
    t = list(tab)
    nb_trie = 1
    while nb_trie < len(t):
        pos = nb_trie
        while pos > 0 : 
            if t[pos-1] > t [pos]:
                t[pos-1], t[pos] = t[pos], t[pos-1]
                pos = pos - 1
            else :
                break
        nb_trie += 1
    return t